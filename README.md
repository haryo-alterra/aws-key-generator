# AWS Key Generator



## Key Generation

Automate your repetitive AWS auth keys generation for CI/CD. To run the script, simply scp the script to the remote AWS server and execute remotely by:

```
sudo bash ./automate_keygen.sh <Public AWS IPv4 Address> <Password for Keys Generated>
```

For example:

```
sudo bash ./automate_keygen.sh ec2-xx-yy-zzz-aaa.compute-1.amazonaws.com password
```

Note:
To keep everything simple, all generated keys share the same given password. This script also re-deploy Docker service installed in remote AWS server to make sure everything is deployment-ready.
