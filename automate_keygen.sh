ec2Link=$1
keyPassword=$2
sudo systemctl enable docker.service
sudo systemctl start docker.service
sudo openssl genrsa -aes256 -out ca-key.pem -passout pass:"$keyPassword" 4096
sudo openssl req -new -x509 -days 365 -key ca-key.pem -sha256 -passin pass:"$keyPassword" -out ca.pem -subj "/C=ID/ST=.../L=.../O=.../OU=.../CN=.../emailAddress=..."
sudo openssl genrsa -out server-key.pem 4096
sudo openssl req -subj "/CN=$ec2Link" -sha256 -new -key server-key.pem -out server.csr
echo subjectAltName = DNS:"$ec2Link",IP:10.10.10.20,IP:127.0.0.1 >> extfile.cnf
echo extendedKeyUsage = serverAuth >> extfile.cnf
sudo openssl x509 -req -days 365 -sha256 -in server.csr -CA ca.pem -CAkey ca-key.pem -CAcreateserial -out server-cert.pem -passin pass:"$keyPassword" -extfile extfile.cnf
sudo openssl genrsa -out key.pem 4096
sudo openssl req -subj '/CN=client' -new -key key.pem -out client.csr
echo extendedKeyUsage = clientAuth > extfile-client.cnf
sudo openssl x509 -req -days 365 -sha256 -in client.csr -CA ca.pem -CAkey ca-key.pem -CAcreateserial -out cert.pem -passin pass:"$keyPassword" -extfile extfile-client.cnf
sudo rm -v client.csr server.csr extfile.cnf extfile-client.cnf
sudo chmod -v 0400 ca-key.pem server-key.pem
sudo chmod -v 0444 ca.pem server-cert.pem cert.pem key.pem
mkdir -p /etc/systemd/system/docker.service.d/
sudo cp ca.pem server-cert.pem server-key.pem /etc/systemd/system/docker.service.d/
whichDockerd=`which dockerd`
optionsConf=/etc/systemd/system/docker.service.d/options.conf
if [ -f "$optionsConf" ]; then
	sudo rm "$optionsConf"
fi
sudo touch "$optionsConf"
echo "[Service]" | sudo tee -a "$optionsConf"
echo "ExecStart=" | sudo tee -a "$optionsConf"
echo "ExecStart=$whichDockerd --tlsverify --tlscacert=/etc/systemd/system/docker.service.d/ca.pem --tlscert=/etc/systemd/system/docker.service.d/server-cert.pem --tlskey=/etc/systemd/system/docker.service.d/server-key.pem -H=0.0.0.0:2376 -H unix:///var/run/docker.sock" | sudo tee -a /etc/systemd/system/docker.service.d/options.conf
sudo systemctl daemon-reload
sudo systemctl restart docker
sudo systemctl status docker
